#!/usr/bin/env python

# Description: Show how epics can be moved between groups, including title, description, labels, child epics and issues.
# Requirements: python-gitlab Python libraries. GitLab API write access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-de/use-cases/gitlab-api
SOURCE_GROUP_ID = os.environ.get('GL_SOURCE_GROUP_ID', 62378643)
# https://gitlab.com/gitlab-de/use-cases/gitlab-api/epic-move-target
TARGET_GROUP_ID = os.environ.get('GL_TARGET_GROUP_ID', 62742177)
# https://gitlab.com/groups/gitlab-de/use-cases/gitlab-api/-/epics/1
EPIC_ID = os.environ.get('GL_EPIC_ID', 1)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
# Goal: Move epic to target group, including title, body, labels, and child epics and issues.
source_group = gl.groups.get(SOURCE_GROUP_ID)
target_group = gl.groups.get(TARGET_GROUP_ID)

# Create a new target epic and copy all its items, then close the source epic.
source_epic = source_group.epics.get(EPIC_ID)
# print(source_epic) #debug

epic_title = source_epic.title
epic_description = source_epic.description
epic_labels = source_epic.labels
epic_issues = source_epic.issues.list()

# Create the epic with minimal attributes
target_epic = target_group.epics.create({
    'title': epic_title,
    'description': epic_description,
})

# Assign the list
target_epic.labels = epic_labels

# Persist the changes in the new epic
target_epic.save()

# Epic issues need to be re-assigned in a loop
for epic_issue in epic_issues:
    ei = target_epic.issues.create({'issue_id': epic_issue.id})

# Child epics need to update their parent_id to the new epic
# Need to search in all epics, use lazy object loading
for sge in source_group.epics.list(lazy=True):
    # this epic has the source epic as parent epic?
    if sge.parent_id == source_epic.id:
        # Update the parent id
        sge.parent_id = target_epic.id
        sge.save()


print("Copied source epic {source_id} ({source_url}) to target epic {target_id} ({target_url})".format(
    source_id=source_epic.id, source_url=source_epic.web_url,
    target_id=target_epic.id, target_url=target_epic.web_url))

# Close the old epic
source_epic.state_event = 'close'
source_epic.save()
print("Closed source epic {source_id} ({source_url})".format(
    source_id=source_epic.id, source_url=source_epic.web_url))