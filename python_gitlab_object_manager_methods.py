#!/usr/bin/env python

# Description: Show how object managers and methods work
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-de/use-cases/
GROUP_ID = os.environ.get('GL_GROUP_ID', 16058698)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
main_group = gl.groups.get(GROUP_ID)

print("Sub groups")
for sg in main_group.subgroups.list():
    print("Subgroup name: {sg}".format(sg=sg.name))

print("Projects (direct)")
for p in main_group.projects.list():
    print("Project name: {p}".format(p=p.name))

print("Projects (including subgroups)")
for p in main_group.projects.list(include_subgroups=True, all=True):
     print("Project name: {p}".format(p=p.name))

print("Issues")
for i in main_group.issues.list(state='opened'):
    print("Issue title: {t}".format(t=i.title))

print("Epics")
for e in main_group.issues.list():
    print("Epic title: {t}".format(t=e.title))

print("Todos")
for t in gl.todos.list(state='pending'):
    print("Todo: {t} url: {u}".format(t=t.body, u=t.target_url))


