import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/everyonecancontribute/observability/o11y.love
PROJECT_ID = os.environ.get('GL_PROJECT_ID', #project_id)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
project = gl.projects.get(PROJECT_ID)
changelog_file_path = 'CHANGELOG.md'  # Define the file path for the changelog

changelog_content = "# Changelog\n"

commits = project.commits.list(ref_name='main', lazy=True, iterator=True)

for commit in commits:
    changelog_content += "- [{text}]({url}) ({name})\n".format(
        text=commit.title,
        url=commit.web_url,
        name=commit.author_name
    )

try:
    # Check if the changelog file exists
    file = project.files.get(file_path=changelog_file_path , ref='main')
    # Update the existing file
    file.content = changelog_content
    file.save(branch='main', commit_message='Update changelog3')
    print(changelog_content)
    print(f'Changelog file updated successfully: {changelog_file_path}')
except gitlab.exceptions.GitlabGetError:
    # Create a new file
    project.files.create({
        'file_path': changelog_file_path,
        'branch': 'main',
        'content': changelog_content,
        'commit_message': 'Create changelog'
    })
    print(f'Changelog file created successfully: {changelog_file_path}')
