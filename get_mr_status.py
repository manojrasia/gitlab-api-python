#!/usr/bin/env python

# Description: Fetch all merge requests from a project id, and print their merge status. Additional debugging with measuring the duration of requests for https://gitlab.com/gitlab-org/gitlab/-/issues/386661#note_1237757295
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys
from timeit import default_timer as timer

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-com/www-gitlab-com
PROJECT_ID = os.environ.get('GL_PROJECT_ID', 7764)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
project = gl.projects.get(PROJECT_ID, lazy=False, pagination="keyset", order_by="updated_at", per_page=100)

mrs = project.mergerequests.list(state='opened', iterator=True, with_merge_status_recheck=True)

for mr in mrs:
    start = timer()
    #print(mr.attributes) #debug
    # https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-request-diffs
    real_mr = project.mergerequests.get(mr.get_id())

    print("- [ ] !{mr_id} - {mr_url}+ Status: {s}, Title: {t}".format(
        mr_id=real_mr.attributes['iid'],
        mr_url=real_mr.attributes['web_url'],
        s=real_mr.attributes['detailed_merge_status'],
        t=real_mr.attributes['title']))

    end = timer()
    duration = end - start
    if duration > 1.0:
        print("ALERT: > 1s ")
    print("> Execution time took {s}s".format(s=(duration)))
